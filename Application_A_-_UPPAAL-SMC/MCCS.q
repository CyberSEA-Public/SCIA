//This file was generated from (Academic) UPPAAL 4.1.24 (rev. 29A3ECA4E5FB0808), November 2019

/*

*/
=============== System Invariants ===============

/*

*/
ES.Begin --> (ES.End && total_time<=M*10)          //INV1. [FALSE/Exceeds max allowed delay of 10000] -> [Delay alert!] Material production could not complete in specified time 

/*

*/
A[] !H.WFM && !P.WFI && !P.WFR                     //INV2. [FALSE] -> [Manipulation confirmed!] System fault states were encountered 

/*

*/
//NO_QUERY

/*

*/
//NO_QUERY

/*

*/
=============== Encounter with Stalled States ===============

/*

*/
E<> H.WFM                                          //SS1: [TRUE] -> The material check failed

/*

*/
E<> P.WFI                                          //SS2: [TRUE] -> The system initialization check failed

/*

*/
E<> P.WFR                                          //SS3: [TRUE] -> The system readiness check failed

/*

*/
//NO_QUERY

/*

*/
//NO_QUERY

/*

*/
=============== SMC QUERIES =================

/*

*/
Pr[total_time<=8640; 1000] (<>processed_mat==864)                   // The Probability of reaching the daily production end within the allocated time (1 day; 1 time unit = 10 seconds)

/*

*/
Pr[total_time<=8280; 1000] (<>processed_mat==864)                   // The Probability of reaching the daily production end in less than the allocated time (23 hours; 1 time unit = 10 seconds)

/*

*/
//NO_QUERY

/*

*/
E [total_time<=360; 1000] (max: processed_mat)                      // Expected hourly production of materials (1 time unit = 10 seconds)

/*

*/
E [total_time<=8640; 1000] (max: processed_mat)                     // Expected daily production of materials (1 time unit = 10 seconds)

/*

*/
//NO_QUERY

/*

*/
//NO_QUERY

/*

*/
E [total_time<=360; 1000] (max: total_delay)                        // Expected total delay in an hour's worth of production (1 time unit = 10 seconds)

/*

*/
E [total_time<=8640; 1000] (max: total_delay)                       // Expected total delay in a day's worth of production (1 time unit = 10 seconds)

/*

*/
//NO_QUERY

/*

*/
//---------------------------------------------------------------------------END OF QUERIES------------------------------------------------------------------------------------------
