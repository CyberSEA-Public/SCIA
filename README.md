# SCIA: A Structured Cyberattack Impact Analysis Approach
This repository contains files related to two applications of the SCIA approach for modeling, visualzing, and analyzing the impact of cyberattacks on ICS or other systems suitable for a modeling and simulation study. 

## Table of contents
* [Description](#description)
* [Technologies](#technologies)
* [Folders](#folders)
* [Files](#files)
* [Setup](#setup)

### Description
This project is an effort to show how to apply the SCIA approach on a manufacturing cell control system (MCCS) with two distinct applications:
* __Application A__: based on formal modeling and verification in UPPAAL-SMC, and 
* __Application B__: based on simulations in MATLAB/Simulink.

Described in an associated research paper, this repository contains the files for the MCCS system models, attacker models capable of performing data tampering and message spoofing attacks on the system, and the associated impact queries for analyzing cyberattack impact.

	
### Technologies
Project is created with:
* [UPPAAL 5.0.0](https://uppaal.org/)
* [MATLAB R2023a](https://www.mathworks.com/products/matlab.html)

### Folders
* __Application A - UPPAAL-SMC__: Contains the files needed to perform SCIA with UPPAAL-SMC
* __Application B - MATLAB-Simulink__: Contains the files needed to perform SCIA with MATLAB/Simulink

### Files
* __MCCS.xml__: The UPPAAL-SMC model file that contains all the described system agents and attackers.
* __MCCS.q__: The UPPAAL-SMC Query file containing all the queries for classical and statistical model checking.
* __manufacturingCell.slx__: The MATLAB/Simulink model file that contains all the described system agents and attackers.
* __manufacturingCellMeasurements.m__: MATLAB/Simulink Script file containing all the queries for model analysis.
	
### Setup
* To run this project with UPPAAL-SMC:
	1. [download](https://uppaal.org/downloads/) and install UPPAAL 5.0.0 (available under a free academic licence). If an error is encountered, run the executable Jar file (uppaal.jar) from the command line instead of the normal executable file (e.g., uppaal.exe).
	2. Open the model file (MCCS.xml) in UPPAAL
	3. Execute the models in the concrete simulator
	4. Import the query file (MCCS.q) if the verifier is empty
	5. Verify the queries in the verifier
* To run this project with MATLAB/Simulink:
	1. [download](https://www.mathworks.com/help/install/ug/install-products-with-internet-connection.html) and install MATLAB R2023a.
	2. Open the model file (manufacturingCell.slx) in MATLAB
	3. Execute the script file (manufacturingCellMeasurements.m)
